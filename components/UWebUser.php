<?php
class UWebUser extends CWebUser
{

    private $_allowedRoutes = array();


    /**
     * Syntax sugar check if current user is superadmin
     * @return bool
     */
    public function isSuperAdmin(){
        return User::checkRole('isSuperAdmin');
    }

    /**
     * isRouteAllowed
     *
     * Checks if current route is in array of allowed routes
     *
     * @param string $route A route to check
     *
     * @return boolean
     */
    public function isRouteAllowed($route)
    {
        $this->_findAllowedRoutes();
        foreach ($this->_allowedRoutes as $allowedRoute) {
            if ($route == $allowedRoute) {
                return true;
            } else {
                // If some controller fully allowed (wildcard)
                if (substr($allowedRoute, -1) == '*') {
                    $routeArray = explode('/', $route);
                    array_splice($routeArray, -1);

                    $allowedRouteArray = explode('/', $allowedRoute);
                    array_splice($allowedRouteArray, -1);

                    if (array_diff($routeArray, $allowedRouteArray) === array())
                        return true;
                }
            }
        }
        return false;
    }

        /**
         * afterLogin 
         */
        protected function afterLogin($fromCookie)
        {
            parent::afterLogin($fromCookie);
            $this->_updateUserState();
            if(method_exists(Yii::app()->getController(), 'afterLoginRedirect')){
                Yii::app()->getController()->afterLoginRedirect();
            }

        }

        /**
         * _updateUserState 
         */
        private function _updateUserState()
        {
                $user = User::model()->active()->with(array('roles','tasks'))->findByPk((int)$this->id);

                if ($user) 
                {
                        $this->name = $user->login;

                        // If it's SuperAdmin
                        if ($user->is_superadmin == 1) 
                                $this->setState('isSuperAdmin', true);

                        // Set roles
                        foreach ($user->roles as $role) 
                                $this->setState($role->code, true);

                        // Set tasks in Yii::app()->user->tasks array
                        $taskArray = array();
                        foreach ($user->tasks as $task) 
                                $taskArray[] = $task->code;

                        foreach ($user->roles as $tRole) 
                        {
                                foreach ($tRole->tasks as $roleTask) 
                                {
                                        $taskArray[] = $roleTask->code;
                                        
                                }
                        }

                        $taskArray = array_unique($taskArray);

                        $this->setState('tasks', $taskArray);
                }
        }

    private function _findAllowedRoutes(){
        if(!$this->_allowedRoutes)
            $this->_allowedRoutes = array_merge($this->_getAllowedUserRoutes(), $this->_getGuestAllowedRoutes());
        return $this->_allowedRoutes;
    }

    /**
     * _getGuestAllowedRoutes
     *
     * @return array
     */
    private function _getGuestAllowedRoutes()
    {
        $cache = $this->_checkCache(true);

        if (is_array($cache)) {
            return $cache;
        } else // If no cached routes
        {
            $sql = "SELECT DISTINCT route FROM user_operation uo
                                INNER JOIN user_task_has_user_operation uthuo ON uo.id = uthuo.user_operation_id
                                INNER JOIN user_task ut ON ut.id = uthuo.user_task_id

                                WHERE ut.code = 'freeAccess'";

            $routes = Yii::app()->db->createCommand($sql)->queryAll();

            $result = array();

            foreach ($routes as $route)
                $result[] = $route['route'];

            $result = array_unique($result);

            // Save results in cache
            $cache->routes = serialize($result);
            $cache->status = 1;
            $cache->is_guest = 1;
            $cache->update_time = time();
            $cache->save(false);

            return $result;
        }
    }

    /**
     * _getAllowedUserRoutes
     *
     * @return array
     */
    private function _getAllowedUserRoutes()
    {
        // No allowed routes if user id isn't set
        if (!Yii::app()->user->id)
            return array();

        $cache = $this->_checkCache();

        if (is_array($cache)) {
            return $cache;
        } else // If no cached routes
        {
            $sqlRoles = "SELECT DISTINCT route FROM user_operation uo
                                INNER JOIN user_task_has_user_operation uthuo ON uo.id = uthuo.user_operation_id
                                INNER JOIN user_task ut ON ut.id = uthuo.user_task_id

                                INNER JOIN user_role_has_user_task urhut ON urhut.user_task_id = ut.id
                                INNER JOIN user_role ur ON ur.id = urhut.user_role_id

                                INNER JOIN user_has_user_role uhur ON uhur.user_role_code = ur.code

                                INNER JOIN user u ON u.id = uhur.user_id

                                WHERE u.id = :user_id";

            $sqlTasks = "SELECT DISTINCT route FROM user_operation uo
                                INNER JOIN user_task_has_user_operation uthuo ON uo.id = uthuo.user_operation_id
                                INNER JOIN user_task ut ON ut.id = uthuo.user_task_id

                                INNER JOIN user_has_user_task uhut ON uhut.user_task_code = ut.code

                                INNER JOIN user u ON u.id = uhut.user_id

                                WHERE u.id = :user_id";

            $userId = Yii::app()->user->id;

            $roleRoutes = Yii::app()->db->createCommand($sqlRoles)
                ->bindParam(':user_id', $userId, PDO::PARAM_INT)
                ->queryAll();

            $taskRoutes = Yii::app()->db->createCommand($sqlTasks)
                ->bindParam(':user_id', $userId, PDO::PARAM_INT)
                ->queryAll();

            $result = array();

            foreach ($roleRoutes as $roleRoute)
                $result[] = $roleRoute['route'];

            foreach ($taskRoutes as $taskRoute)
                $result[] = $taskRoute['route'];

            $result = array_unique($result);

            // Save results in cache
            $cache->routes = serialize($result);
            $cache->status = 1;
            $cache->is_guest = 0;
            $cache->update_time = time();
            $cache->save(false);

            return $result;
        }
    }

    /**
     * _checkCache
     *
     * Get agailiable routes from cache (or UserCache model)
     *
     * @param boolean $forGuest
     *
     * @return array / CActiveRecord
     */
    private function _checkCache($forGuest = false)
    {
        if ($forGuest) {
            $userCache = UserCache::model()->find('is_guest = 1');
        } else {
            $user = User::model()->active()->with('cache')->findByPk((int)Yii::app()->user->id);

            // If no user (let's say he's been banned during session)
            // then return empty array - no allowed routes
            if (!$user)
                return array();

            $userCache = $user->cache;
        }

        // No cache
        if (!$userCache) {
            $cache = new UserCache;

            if (!$forGuest)
                $cache->user_id = $user->id;

            return $cache;
        } // Has been modified
        elseif ($userCache->status == 0) {
            return $userCache;
        } // Expired
        elseif ($userCache->update_time < (time() - Yii::app()->getModule('UserAdmin')->cache_time)) {
            return $userCache;
        } // All OK - so we return array of routes
        else {
            return unserialize($userCache->routes);
        }
    }
}
