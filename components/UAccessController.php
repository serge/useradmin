<?php
class UAccessController extends CController
{
    public $freeAccess = false;
    public $freeAccessActions = array();

    public function filters()
    {
        return array(
            'userAdminControl'
        );
    }

    /**
     * filterUserAdminControl
     *
     * @param mixed $filterChain
     * @return void
     */
    public function filterUserAdminControl($filterChain)
    {
        // Get login action url
        if (is_array(Yii::app()->user->loginUrl))
            $loginUrl = trim(Yii::app()->user->loginUrl[0], '/');
        else
            $loginUrl = trim(Yii::app()->user->loginUrl, '/');


        $errorAction = trim(Yii::app()->errorHandler->errorAction, '/');


        // If it's not error or login action
        if ((strtolower($this->route) === strtolower($loginUrl)) OR (strtolower($this->route) === strtolower($errorAction))) {
            $filterChain->run();
        } // If this controller or this action if free to access for everyone
        elseif (($this->freeAccess === true) OR (in_array($this->action->id, $this->freeAccessActions))) {
            $filterChain->run();
        }
        // If user is SuperAdmin
        elseif (User::checkRole('isSuperAdmin')) {
            $filterChain->run();
        } // Check if this user has access to this action
        else {
            if (Yii::app()->getUser()->isRouteAllowed($this->route))
                $filterChain->run();
            else
                $this->_accessDeniedAction();
        }
    }

    public function afterLoginRedirect(){
        if(Yii::app()->session[Yii::app()->getModule('UserAdmin')->redirect_cookie_name]){
            $redir = Yii::app()->session[Yii::app()->getModule('UserAdmin')->redirect_cookie_name];
            unset(Yii::app()->session[Yii::app()->getModule('UserAdmin')->redirect_cookie_name]);
            $this->redirect($redir);
            }
    }

    private function _accessDeniedAction(){
        Yii::app()->session[Yii::app()->getModule('UserAdmin')->redirect_cookie_name] = Yii::app()->getBaseUrl(1). '/'.$this->route;
        if(Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->user->loginUrl);
        else
            throw new CHttpException(403, Yii::t("UserAdminModule.front", "You are not authorized to perform this action."));
    }

}
